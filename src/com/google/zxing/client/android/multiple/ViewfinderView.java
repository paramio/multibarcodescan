/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android.multiple;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.client.android.multiple.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder
 * rectangle and partial transparency outside it, as well as the laser scanner
 * animation and result points. 
 * 
 * <br/><br/>
 * 该视图是覆盖在相机的预览视图之上的一层视图。扫描区构成原理，其实是在预览视图上画四块遮罩层，
 * 中间留下的部分保持透明，并画上一条激光线，实际上该线条就是展示而已，与扫描功能没有任何关系。
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class ViewfinderView extends View {

    private static final int[] SCANNER_ALPHA = {
            0, 64, 128, 192, 255, 192, 128, 64
    };
    
    private long ANIMATION_DELAY = 20L;

    private static final int CURRENT_POINT_OPACITY = 0xA0;

    private static final int MAX_RESULT_POINTS = 20;

    private static final int POINT_SIZE = 6;

    private CameraManager cameraManager;

    private final Paint paint;

    private Bitmap resultBitmap;

    private final int maskColor;

    private final int resultColor;

    private final int laserColor;

    private final int resultPointColor;

    private int scannerAlpha;

    private List<ResultPoint> possibleResultPoints;

    private List<ResultPoint> lastPossibleResultPoints;
    
    private static final int SPEEN_DISTANCE = 5;      //中间的划线移动距离
    
    private int middle;
    boolean isFirst;
    /** 
     * 四个绿色边角对应的长度 
     */  
    private int ScreenRate;  
      
    /** 
     * 四个绿色边角对应的宽度 
     */  
    private static final int CORNER_WIDTH = 10;  
    /** 
     * 手机的屏幕密度 
     */  
    private static float density;  
    
    public static float PREVIEW_CENTER;
    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        density = context.getResources().getDisplayMetrics().density;  
        //将像素转换成dp  
        ScreenRate = (int)(20 * density);  
  
        // Initialize these once for performance rather than calling them every
        // time in onDraw().
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.viewfinder_mask);
        resultColor = resources.getColor(R.color.result_view);
        laserColor = resources.getColor(R.color.viewfinder_laser);
        resultPointColor = resources.getColor(R.color.possible_result_points);
        scannerAlpha = 0;
        possibleResultPoints = new ArrayList<ResultPoint>(5);
        lastPossibleResultPoints = null;
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (cameraManager == null) {
            return; // not ready yet, early draw before done configuring
        }
        Rect frame = cameraManager.getFramingRect();
        Rect previewFrame = cameraManager.getFramingRectInPreview();
        if (frame == null || previewFrame == null) {
            return;
        }
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        if(!isFirst){
        	//首次运行时初始化 防止之后 刷新是再次初始化
        	 middle =  frame.top;
        }
        isFirst = true;
        // Draw the exterior (i.e. outside the framing rect) darkened
        paint.setColor(resultBitmap != null ? resultColor : maskColor);
        canvas.drawRect(0, 0, width, frame.top, paint);
        Log.d("width",String.valueOf(width));
        Log.d("frame.top",String.valueOf(frame.top));
        Log.d("frame.left",String.valueOf(frame.left));
        Log.d("frame.bottom",String.valueOf(frame.bottom));
        Log.d("frame.rignt",String.valueOf(frame.right));
        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1, paint);
        canvas.drawRect(0, frame.bottom + 1, width, height, paint);
        //draw 4 corner
        paint.setColor(Color.LTGRAY);
        canvas.drawRect(frame.left, frame.top, frame.left + ScreenRate,  
                frame.top + CORNER_WIDTH, paint);  
        canvas.drawRect(frame.left, frame.top, frame.left + CORNER_WIDTH, frame.top  
                + ScreenRate, paint);  
        canvas.drawRect(frame.right - ScreenRate, frame.top, frame.right+1,  
                frame.top + CORNER_WIDTH, paint);  
        canvas.drawRect(frame.right - CORNER_WIDTH+1, frame.top, frame.right+1, frame.top  
                + ScreenRate, paint);  
        canvas.drawRect(frame.left, frame.bottom - CORNER_WIDTH, frame.left  
                + ScreenRate, frame.bottom, paint);  
        canvas.drawRect(frame.left, frame.bottom - ScreenRate,  
                frame.left + CORNER_WIDTH, frame.bottom, paint);  
        canvas.drawRect(frame.right - ScreenRate+1, frame.bottom - CORNER_WIDTH+1,  
                frame.right+1, frame.bottom+1, paint);  
        canvas.drawRect(frame.right - CORNER_WIDTH+1, frame.bottom - ScreenRate+1,  
                frame.right+1, frame.bottom+1, paint); 
        if (resultBitmap != null) {
            // Draw the opaque result bitmap over the scanning rectangle
            paint.setAlpha(CURRENT_POINT_OPACITY);
            canvas.drawBitmap(resultBitmap, null, frame, paint);
        } else {      
        	ANIMATION_DELAY = 0L;
            paint.setColor(Color.LTGRAY);
            //paint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
            //scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;            
            middle += SPEEN_DISTANCE;    //移动中间扫描线
            if(middle >= frame.bottom)
            {
            	middle = frame.top;
            }
            canvas.drawRect(frame.left + 50, middle - 1, frame.right - 50, middle + 2, paint);
            if(middle >= frame.bottom-100){
            	ANIMATION_DELAY = 30L;
            }
            // Request another update at the animation interval, but only
            // repaint the laser line,
            // not the entire viewfinder mask.
            postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top,frame.right, frame.bottom );
        }
    }

    public void drawViewfinder() {
        Bitmap resultBitmap = this.resultBitmap;
        this.resultBitmap = null;
        if (resultBitmap != null) {
            resultBitmap.recycle();
        }
        invalidate();
    }

    /**
     * Draw a bitmap with the result points highlighted instead of the live
     * scanning display.
     * 
     * @param barcode An image of the decoded barcode.
     */
    public void drawResultBitmap(Bitmap barcode) {
        resultBitmap = barcode;
        invalidate();
    }

    public void addPossibleResultPoint(ResultPoint point) {
        List<ResultPoint> points = possibleResultPoints;
        synchronized (points) {
            points.add(point);
            int size = points.size();
            if (size > MAX_RESULT_POINTS) {
                // trim it
                points.subList(0, size - MAX_RESULT_POINTS / 2).clear();
            }
        }
    }

}
