/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android.multiple;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.camera.AutoFocusManager;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.client.android.multiple.R;
import com.google.zxing.client.android.result.ResultHandler;
import com.google.zxing.client.android.result.ResultHandlerFactory;


/**
 * This activity opens the camera and does the actual scanning on a background
 * thread. It draws a viewfinder to help the user place the barcode correctly,
 * shows feedback as the image processing is happening, and then overlays the
 * results when a scan is successful.
 * 
 * 此Activity所做的事：
 * 1. 开启camera，在后台独立线程中完成扫描任务；
 * 2. 绘制了一个扫描区（viewfinder）来帮助用户将条码置于其中以准确扫描；
 * 3. 扫描成功后会将扫描结果展示在界面上。
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen
 */
public final class CaptureActivity extends Activity implements Camera.AutoFocusCallback,SurfaceHolder.Callback {

    private static final String TAG = CaptureActivity.class.getSimpleName();

    public static final int HISTORY_REQUEST_CODE = 0x0000bacc;

    private CameraManager cameraManager;

    private CaptureActivityHandler handler;

    private Result savedResultToShow;

    private ViewfinderView viewfinderView;

    /**
     * 扫描提示，例如"请将条码置于取景框内扫描"之类的提示
     */
    private TextView statusView;

    /**
     * 扫描结果展示窗口
     */
    private View resultView;

    private boolean hasSurface;
    


    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 编码类型，该参数告诉扫描器采用何种编码方式解码，即EAN-13，QR Code等等
     * 对应于DecodeHintType.POSSIBLE_FORMATS类型
     * 参考DecodeThread构造函数中如下代码：hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
     */
    private Collection<BarcodeFormat> decodeFormats;

    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 字符集，告诉扫描器该以何种字符集进行解码
     * 对应于DecodeHintType.CHARACTER_SET类型
     * 参考DecodeThread构造器如下代码：hints.put(DecodeHintType.CHARACTER_SET, characterSet);
     */
    private String characterSet;
    
    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 该参数最终会传入MultiFormatReader，上面的decodeFormats和characterSet最终会先加入到decodeHints中
     * 最终被设置到MultiFormatReader中
     * 参考DecodeHandler构造器中如下代码：multiFormatReader.setHints(hints);
     */
    private Map<DecodeHintType, ?> decodeHints;

  

    /**
     * 活动监控器。如果手机没有连接电源线，那么当相机开启后如果一直处于不被使用状态则该服务会将当前activity关闭。
     * 活动监控器全程监控扫描活跃状态，与CaptureActivity生命周期相同。每一次扫描过后都会重置该监控，即重新倒计时。
     */
    private InactivityTimer inactivityTimer;

    /**
     * 声音震动管理器。 扫描成功后可以播放提示音并震动，这两种功能都是用户自定义的。
     * 在Barcode Scanner中点击菜单键，点设置即可看到这两项的设置
     */
    private BeepManager beepManager;


    /**
     * 获取条形码显示结果内容布局，用于添加 和 清空
     */
    private LinearLayout content_layout;
    /**
     * 定时关闭结果处理机制
     */
	private Handler result_handler;

	
	protected boolean isFirst;
    /**
     * sd卡根目录变量
     */	
	private String SDcardPath;
	/**
	 * 存储rawresult
	 *
	 */
	private List<RawResult> resultsList;

	/**
	 * list项数量
	 * @return
	 */
	private int RESULTS_NUM = 5;
	
	private String FILE_NAME = "SN";
	
    ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    CameraManager getCameraManager() {
        return cameraManager;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // 在扫描功能开启后，保持屏幕处于点亮状态
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);

        // 这里仅仅是对各个组件进行简单的创建动作，真正的初始化动作放在onResume中
        hasSurface = false;
        // 获取SD卡根目录
        SDcardPath = Environment.getExternalStorageDirectory().getPath();
        resultsList = new ArrayList<RawResult>();
        inactivityTimer = new InactivityTimer(this);
        beepManager = new BeepManager(this);
       
    }

    @SuppressLint("HandlerLeak")
	@Override
    protected void onResume() {
        super.onResume();
        isFirst = false;
        //初始化resultview 清除textview
        removeResultsTextView();
        // CameraManager must be initialized here, not in onCreate(). This is
        // necessary because we don't
        // want to open the camera driver and measure the screen size if we're
        // going to show the help on
        // first launch. That led to bugs where the scanning rectangle was the
        // wrong size and partially
        // off screen.
        /**
         * 相机初始化的动作需要开启相机并测量屏幕大小，这些操作
         * 不建议放到onCreate中，因为如果在onCreate中加上首次启动展示帮助信息的代码的
         * 话，会导致扫描窗口的尺寸计算有误的bug
         */
        cameraManager = new CameraManager(getApplication());

        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        viewfinderView.setCameraManager(cameraManager);
        resultView = findViewById(R.id.result_view);
        statusView = (TextView) findViewById(R.id.status_view);

        handler = null;

        /* 重置状态窗口，扫描窗口和结果窗口的状态
         * 修改之前的
        //resetStatusView();
         * *
         */
        //初始化各个view  并给 resultView添加touch事件
        initViewEvent();
        result_handler = new Handler(){
        	@Override
			public void handleMessage(Message msg){         // handle message  
                switch (msg.what) {  
                case 1:  
                	restartPreviewAfterDelay(1000);
    				findViewById(R.id.result_view_sub).setVisibility(View.VISIBLE);    				
    				findViewById(R.id.result_view_textclick).setVisibility(View.GONE);
                }  
      
                super.handleMessage(msg);  
            }  
        };
        // 摄像头预览功能必须借助SurfaceView，因此也需要在一开始对其进行初始化
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            // The activity was paused but not stopped, so the surface still
            // exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            initCamera(surfaceHolder);
        } else {
            // Install the callback and wait for surfaceCreated() to init the
            // camera.
            // 如果SurfaceView已经渲染完毕，会回调surfaceCreated，在surfaceCreated中调用initCamera()
            surfaceHolder.addCallback(this);
        }

        // 加载声音配置，其实在BeemManager的构造器中也会调用该方法，即在onCreate的时候会调用一次
        beepManager.updatePrefs();
        

        // 恢复活动监控器
        inactivityTimer.onResume();

    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		removeResultsTextView();
	}


    @Override
    protected void onPause() {
    	isFirst = false;
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        
        // 暂停活动监控器
        inactivityTimer.onPause();
        
        
        // 关闭摄像头
        cameraManager.closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // 停止活动监控器    	
        inactivityTimer.shutdown();
        super.onDestroy();
        
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
//            case KeyEvent.KEYCODE_BACK: // 拦截返回键
//            	removeResultsTextView();              
//                restartPreviewAfterDelay(0L);
//                return true;                
                
            case KeyEvent.KEYCODE_FOCUS:
            case KeyEvent.KEYCODE_CAMERA:
                // Handle these events so they don't launch the Camera app
                return true;
                // Use volume up/down to turn on light
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                cameraManager.setTorch(false);
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                cameraManager.setTorch(true);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	menu.close();
        
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
 
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        
    }

    /**
     * 向CaptureActivityHandler中发送消息，并展示扫描到的图像
     * 
     * @param bitmap
     * @param result
     */
    private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
        // Bitmap isn't used yet -- will be used soon
        if (handler == null) {
            savedResultToShow = result;
        } else {
            if (result != null) {
                savedResultToShow = result;
            }
            if (savedResultToShow != null) {
                Message message = Message.obtain(handler, R.id.decode_succeeded, savedResultToShow);
                handler.sendMessage(message);
            }
            savedResultToShow = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }
    }
    

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    /**
     * A valid barcode has been found, so give an indication of success and show
     * the results.
     * 
     * @param scaleFactor amount by which thumbnail was scaled
     * @param barcode A greyscale bitmap of the camera data which was decoded.
     */
    public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        inactivityTimer.onActivity();
        // 解析rawResult，根据不同类型result生成对应的ResultHandler
        ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(this, rawResult);

        boolean fromLiveScan = barcode != null;
        if (fromLiveScan) {
            // Then not from history, so beep/vibrate and we have an image to
            // draw on
            //beepManager.playBeepSoundAndVibrate(); //扫描成功鸣叫
            //drawResultPoints(barcode, scaleFactor, rawResult);
        }

        handleDecodeInternally(rawResult, resultHandler, barcode);
       
    }

    /**
     * Superimpose a line for 1D or dots for 2D to highlight the key features of
     * the barcode.
     * 
     * @param barcode A bitmap of the captured image.
     * @param scaleFactor amount by which thumbnail was scaled
     * @param rawResult The decoded results which contains the points to draw.
     */
    private void drawResultPoints(Bitmap barcode, float scaleFactor, Result rawResult) {
        ResultPoint[] points = rawResult.getResultPoints();
        if (points != null && points.length > 0) {
            Canvas canvas = new Canvas(barcode);
            Paint paint = new Paint();
            paint.setColor(getResources().getColor(R.color.result_points));
            if (points.length == 2) {
                paint.setStrokeWidth(4.0f);
                drawLine(canvas, paint, points[0], points[1], scaleFactor);
            } else if (points.length == 4
                    && (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A || rawResult
                            .getBarcodeFormat() == BarcodeFormat.EAN_13)) {
                // Hacky special case -- draw two lines, for the barcode and
                // metadata
                drawLine(canvas, paint, points[0], points[1], scaleFactor);
                drawLine(canvas, paint, points[2], points[3], scaleFactor);
            } else {
                paint.setStrokeWidth(10.0f);
                for (ResultPoint point : points) {
                    if (point != null) {
                        canvas.drawPoint(scaleFactor * point.getX(), scaleFactor * point.getY(),
                                paint);
                    }
                }
            }
        }
    }

    private static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b,
            float scaleFactor) {
        if (a != null && b != null) {
            canvas.drawLine(scaleFactor * a.getX(), scaleFactor * a.getY(), scaleFactor * b.getX(),
                    scaleFactor * b.getY(), paint);
        }
    }

    // Put up our own UI for how to handle the decoded contents.
    /**
     * 该方法会将最终处理的结果展示到result_view上。并且如果选择了"检索更多信息"则会内部对结果进行进一步的查询
     * 
     * @param rawResult
     * @param resultHandler
     * @param barcode
     */
    private void handleDecodeInternally(Result rawResult, ResultHandler resultHandler,
            Bitmap barcode) {

    	//检索各个条码
    	String SN= null;
    	String IMEI = null;
    	String ICCID = null;
    	
    	String fileName = null;
    	String format = rawResult.getBarcodeFormat().toString();
    	String content = resultHandler.getDisplayContents().toString();
    	
    	Log.d("_results", format+":"+content);
    	
    	//将rawResult的类型和内容 循环添加进列表
    	resultsList.add(new RawResult(format,content));
    	
    	if(resultsList.size() >= RESULTS_NUM){
    		//判断获得的列表 项目数是否大于等于5条
	        statusView.setVisibility(View.GONE);
	        viewfinderView.setVisibility(View.GONE);
	        resultView.setVisibility(View.VISIBLE);
	
	        ImageView barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
	        if (barcode == null) {
	            barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),
	                    R.drawable.launcher_icon));
	        } else {
	        	// 在resultsView中展示图片
	            barcodeImageView.setImageBitmap(barcode);
	            
	            for(RawResult rawresult:resultsList){
            		if(rawresult.getContent().trim().charAt(0) == 'S')
            		{
            			SN = rawresult.getContent(); 
            		}else if (rawresult.getContent().trim().length() == 20){
            			ICCID = rawresult.getContent(); 
            		}else if (rawresult.getContent().trim().length() == 15){
            			IMEI = rawresult.getContent(); 
            		}
            	}
	            if(FILE_NAME == "IMEI" ){
	            	fileName = IMEI;
	            }else if (FILE_NAME == "SN"){
	            	fileName = SN;
	            }
	            // 将图片存储到指定目录下
	            
	            String _filePath = SDcardPath+"/MultiBarScanner/";
	            String txtPath = _filePath+"index.txt";
	            try {
					saveBitmapToFile(barcode, _filePath+fileName+".jpg");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            Date date =new Date();
	            String _date =dateformat.format(date);
	            writeFileSdcard(txtPath, _date+":"+FILE_NAME+" Model# \t"+IMEI+"\t"+SN+"\t"+ICCID+"\n\n");
	        }
	        
	        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
	        TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
	        timeTextView.setText(formatter.format(new Date(rawResult.getTimestamp())));
	
	        content_layout = (LinearLayout)findViewById(R.id.contents_Layout);
	        for(RawResult _result : resultsList ){	        
	        TextView contentsTextView = new TextView(content_layout.getContext());        
	        contentsTextView.setTextSize(20L);
	        contentsTextView.setText(_result.getFormat()+":"+_result.getContent());
	        content_layout.addView(contentsTextView);
	        contentsTextView = null;	        
	        }
	        
	        content_layout = null;
	        resultsList.clear();    
	        //定时3秒 返回扫描界面
	        Message message = result_handler.obtainMessage(1);     // Message  
	        result_handler.sendMessageDelayed(message,3000);  
	        Log.d("CpatureActivity","send Message success!");
    	}
       
    }

    @Override
	public void closeContextMenu() {
		// TODO Auto-generated method stub
    	
		super.closeContextMenu();
	}

	@Override
	public void onContextMenuClosed(Menu menu) {
		// TODO Auto-generated method stub
		
		super.onContextMenuClosed(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	// Briefly show the contents of the barcode, then handle the result outside
    // Barcode Scanner.
    
    private void sendReplyMessage(int id, Object arg, long delayMS) {
        if (handler != null) {
            Message message = Message.obtain(handler, id, arg);
            if (delayMS > 0L) {
                handler.sendMessageDelayed(message, delayMS);
            } else {
                handler.sendMessage(message);
            }
        }
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (cameraManager.isOpen()) {
            Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
            return;
        }
        try {
            cameraManager.openDriver(surfaceHolder);
            // Creating the handler starts the preview, which can also throw a
            // RuntimeException.
            if (handler == null) {
                handler = new CaptureActivityHandler(this, decodeFormats, decodeHints,
                        characterSet, cameraManager);
            }
            decodeOrStoreSavedBitmap(null, null);
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
            displayFrameworkBugMessageAndExit();
        } catch (RuntimeException e) {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            Log.w(TAG, "Unexpected error initializing camera", e);
            displayFrameworkBugMessageAndExit();
        }
    }
    /**
     * 清除 resultview里的 textView
     */
    public void removeResultsTextView(){
    	content_layout = (LinearLayout)findViewById(R.id.contents_Layout);
        //content_layout.removeAllViews();
        int count = content_layout.getChildCount();
        Log.d("chidren",String.valueOf(count));
        if(count != 0){
        	content_layout.removeViews(0, count);
        }
    }
    @Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		removeResultsTextView();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		removeResultsTextView();
	}

	private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    /**
     * 在经过一段延迟后重置相机以进行下一次扫描。
     * 成功扫描过后可调用此方法立刻准备进行下次扫描
     * 
     * @param delayMS
     */
    public void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
        }
       
        resetStatusView();
    }

    /**
     * 展示状态视图和扫描窗口，隐藏结果视图
     */
    private void resetStatusView() {
       resultView.setVisibility(View.GONE);
        statusView.setText(R.string.msg_default_status);
        statusView.setVisibility(View.VISIBLE);
        viewfinderView.setVisibility(View.VISIBLE);
        removeResultsTextView();
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }
    
    /**
     * 初始化预览界面 添加触摸事件进入扫描状态
     */
    public void	initViewEvent(){
    	findViewById(R.id.result_view_sub).setVisibility(View.INVISIBLE);
    	resultView.setVisibility(View.VISIBLE);
    	statusView.setText("");
        statusView.setVisibility(View.GONE);
        viewfinderView.setVisibility(View.GONE);
        findViewById(R.id.result_view_textclick).setVisibility(View.VISIBLE);
        //隐藏 result里的view
        
        //findViewById(R.id.result_button_view).setVisibility(ViewGroup.INVISIBLE);
    	((LinearLayout)findViewById(R.id.result_view)).setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				if(!isFirst){
					restartPreviewAfterDelay(1000);
					findViewById(R.id.result_view_sub).setVisibility(View.VISIBLE);
					//findViewById(R.id.result_button_view).setVisibility(ViewGroup.VISIBLE);
					findViewById(R.id.result_view_textclick).setVisibility(View.GONE);
					isFirst = true;
				}
				return true;
			}
		});
    	viewfinderView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				cameraManager.getCamera().autoFocus(CaptureActivity.this);
				return true;
			}
		});
    	
    	((ToggleButton)findViewById(R.id.toggle_save)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//通过点击togglebutton 切换保存图片的模式	
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					FILE_NAME = "IMEI";
				}else{
					FILE_NAME = "SN";
				}
			}
		});
    }

	@Override
	public void onAutoFocus(boolean arg0, Camera arg1) {
		// TODO Auto-generated method stub
		cameraManager.getCamera().cancelAutoFocus();
	}
	
	  /**
     * 写， 读sdcard目录上的文件，要用FileOutputStream， 不能用openFileOutput
     * 不同点：openFileOutput是在raw里编译过的，FileOutputStream是任何文件都可以
     * @param fileName
     * @param message
     */ 
    // 写入/mnt/sdcard/index.txt 
    public void writeFileSdcard(String filePath, String message) {  
        try { 
        	//FileOutputStream(String name, boolean append)
            FileOutputStream fout = new FileOutputStream(filePath,true);  
            byte[] bytes = message.getBytes(); 
            fout.write(bytes); 
            fout.close();  
        }  
        catch (Exception e) {  
            e.printStackTrace(); 
        }  
    } 
	
	/**
     * Save Bitmap to a file.保存图片到SD卡。
     * 
     * @param bitmap
     * @param file
     * @return error message if the saving is failed. null if the saving is
     *         successful.
     * @throws IOException
     */
    public static void saveBitmapToFile(Bitmap bitmap, String _file)
            throws IOException {
        BufferedOutputStream os = null;
        try {
            File file = new File(_file);
            // String _filePath_file.replace(File.separatorChar +
            // file.getName(), "");
            int end = _file.lastIndexOf(File.separator);
            String _filePath = _file.substring(0, end);
            File filePath = new File(_filePath);
            if (!filePath.exists()) {
                filePath.mkdirs();
            }
            file.createNewFile();
            os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("save images", e.getMessage(), e);
                }
            }
        }
    }
}
